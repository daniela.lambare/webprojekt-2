-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 08. Dez 2020 um 15:38
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `notebooks`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `benutzername` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `passwort` text COLLATE utf8mb4_bin NOT NULL,
  `Vorname` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `Nachname` text COLLATE utf8mb4_bin NOT NULL,
  `rechte` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Daten für Tabelle `admins`
--

INSERT INTO `admins` (`id`, `benutzername`, `passwort`, `Vorname`, `Nachname`, `rechte`) VALUES
(1, 'gi.oel', 'Costa', 'Gioele', 'Petrillo', 2),
(2, 'md', 'dummheit', 'Daniela', 'Lambaré', 2),
(3, 'admin', 'root', 'Admin', 'Root', 1),
(9, 'shahmir', 'dummkopf', 'Shahmir', 'Salem', 3),
(12, 'dani', 'DaniDani', 'Daniela', 'Lambaré', 2),
(13, 'daniela', 'hallo123', 'Daniela', 'lambare', 3),
(14, 'marc', 'mmaa', 'Marco', 'Petrillo', 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `notebooks`
--

CREATE TABLE `notebooks` (
  `id` int(2) NOT NULL,
  `marke` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `aufloesung` varchar(100) DEFAULT NULL,
  `zoll` varchar(100) DEFAULT NULL,
  `prozessor` varchar(100) DEFAULT NULL,
  `RAM` int(2) DEFAULT NULL,
  `USB_anzahl` int(1) DEFAULT NULL,
  `USB-C_anzahl` int(1) DEFAULT NULL,
  `windows_typ` varchar(100) DEFAULT NULL,
  `CPU_kerne` int(1) DEFAULT NULL,
  `CPU_tkt_frequenz` varchar(3) DEFAULT NULL,
  `akkulaufzeit` varchar(100) DEFAULT NULL,
  `aktivstift` tinyint(1) DEFAULT NULL,
  `touchscreen` tinyint(1) DEFAULT NULL,
  `convertible` tinyint(1) DEFAULT NULL,
  `GPU_marke` varchar(100) DEFAULT NULL,
  `GPU_model` varchar(100) DEFAULT NULL,
  `videoanschluss` varchar(18) DEFAULT NULL,
  `SD` int(1) DEFAULT NULL,
  `SSD` int(3) DEFAULT NULL,
  `preis` int(4) DEFAULT NULL,
  `klasse` text NOT NULL,
  `beschreibung` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `notebooks`
--

INSERT INTO `notebooks` (`id`, `marke`, `model`, `aufloesung`, `zoll`, `prozessor`, `RAM`, `USB_anzahl`, `USB-C_anzahl`, `windows_typ`, `CPU_kerne`, `CPU_tkt_frequenz`, `akkulaufzeit`, `aktivstift`, `touchscreen`, `convertible`, `GPU_marke`, `GPU_model`, `videoanschluss`, `SD`, `SSD`, `preis`, `klasse`, `beschreibung`) VALUES
(1, 'Lenovo', 'Yoga C740-15IML', 'Full HD', '15.6', 'i7-10510U', 16, 2, 2, 'Home', 4, '1.8', '15', 1, 1, 1, 'Intel', 'UHD Graphics', '', 0, 512, 1210, 'IMS', ''),
(2, 'Lenovo', 'Yoga C740-14IML', 'Full HD', '14', 'i7-10510U', 16, 2, 2, 'Home', 4, '1.8', '13', 1, 1, 1, 'Intel', 'UHD Graphics', '', 0, 512, 1200, 'IMS', ''),
(3, 'Lenovo', 'ThinkPad X1 Extreme G2', '4K', '15.6', 'i7-9750H', 16, 2, 2, 'Pro', 6, '2.6', '14', 1, 1, 0, 'Nvidia', 'GTX 1650 Max-Q', 'HDMI', 1, 512, 2650, 'IMS', ''),
(4, 'Lenovo', 'IdeaPad C340-14IML', 'Full HD', '14', 'i5-10210U', 8, 2, 1, 'Home', 4, '1.6', '10.5', 1, 1, 1, 'Intel', 'UHD Graphics', 'HDMI', 1, 512, 730, 'Restliche', ''),
(5, 'Lenovo', 'Ideapad C340-15IIL', 'Full HD', '15.6', 'i5-1035G1', 8, 2, 1, 'Home', 4, '1', '12.5', 1, 1, 1, 'Intel', 'UHD Graphics', 'HDMI', 0, 256, 850, 'Restliche', ''),
(6, 'HP', 'Envy x360 15-dr1230ng', 'Full HD', '15.6', 'i7-10510U', 8, 2, 1, '', 4, '1.8', '14.75', 1, 1, 1, 'Intel', 'UHD Graphics', 'HDMI', 0, 512, 1050, 'IMS', ''),
(7, 'HP', 'ProBook x360 440 G1 - 4QW71EA ', 'Full HD', '14', 'i7-8550U', 16, 1, 3, 'Pro', 4, '1.8', '13', 1, 1, 1, 'Intel', 'UHD Graphics 620', 'HDMI, Display Port', 1, 512, 1220, 'IMS', ''),
(8, 'HP', 'Envy x360 15-e0502nz', 'Full HD', '15.6', 'i5-1035G1', 8, 2, 1, 'Home', 4, '1', '8.5', 1, 1, 1, 'Intel', 'UHD Graphics', 'HDMI', 1, 256, 949, 'Restliche', ''),
(9, 'HP', 'Envy x360 15-eDR1214NG', '4K', '15.6', 'i5-10210U', 16, 2, 1, 'Home', 4, '1.6', '15.7', 1, 1, 1, 'Nvidia', 'MX250', 'HDMI', 0, 256, 1230, 'Restliche', ''),
(10, 'HP', 'EliteBook x360 1040 G6', 'Full HD', '14', 'i7-8565U', 16, 2, 2, 'Pro', 4, '1.8', '24', 1, 1, 1, 'Intel', 'UHD Graphics 620', 'HDMI', 0, 512, 1930, 'IMS', ''),
(11, 'ASUS', 'ZenBook Flip 14 - UX463FA - AI039T', 'Full HD', '14', 'i5-10210U', 8, 2, 1, 'Home', 4, '1.6', '13', 1, 1, 1, 'Intel', 'UHD Graphics', 'HDMI', 1, 512, 1110, 'Restliche', ''),
(12, 'ASUS', 'ZenBook Flip 14 - UX463FA - AI039R', 'Full HD', '14', 'i5-10210U', 8, 2, 1, 'Pro', 4, '1.6', '13', 1, 1, 1, 'Intel', 'UHD Graphics', 'HDMI', 1, 512, 1250, 'Restliche', ''),
(13, 'ASUS', 'VivoBook Flip 14 TP412FA', 'Full HD', '14', 'i7-8565U', 16, 3, 1, 'Home', 4, '1.8', '6.4', 1, 1, 1, 'Intel', 'UHD Graphics', 'HDMI', 0, 512, 1160, 'IMS', ''),
(14, 'Dell', 'Latitude 7400', 'Full HD', '14', 'i5-8265U', 8, 2, 2, 'Pro', 4, '1.6', '13', 1, 1, 1, 'Intel', 'UHD Graphics 620', '', 0, 256, 1480, 'Restliche', ''),
(15, 'Dell', 'Latitude 7400-0D26H', 'Full HD', '14', 'i7-8665U', 16, 2, 2, 'Pro', 4, '1.8', '13', 1, 1, 1, 'Intel', 'UHD Graphics 620', '', 0, 512, 2010, 'IMS', ''),
(16, 'Dell', 'Precision 5530', 'Full HD', '15.6', 'i7-8706G', 16, 2, 2, 'Pro', 4, '3.1', '11', 1, 1, 1, 'AMD', 'Radeon RX Vega M GL Graphics', '', 1, 512, 2300, 'IMS', '');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `notebooks`
--
ALTER TABLE `notebooks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT für Tabelle `notebooks`
--
ALTER TABLE `notebooks`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
