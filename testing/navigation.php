<!-- Navgiationsmenu -->
<!--Stylesheets-->

<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="Bootstrap/css/bootstrap.css">


<!--Logo-->
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navigation">
    <a href="https://www.bwz-rappi.ch">
        <img alt="BWZ-Compare" src="bilder/bwz_logo.png" width="55" height="40">
    </a>
    <a class="navbar-brand" href="home.php">BWZ-Compare</a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="home.php">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ueber_uns.php">Team</a>
            </li>
            <!--Dropdown-->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="klasse.php" id="navbarDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Klassen
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="techK.php">Technische Klassen</a>
                    <a class="dropdown-item" href="restK.php">Sonstige</a>
                </div>
            </li>
        </ul>

        <!--Suchfeld-->
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" style="width: 500px;">
            <button class="btn btn-outline-success my-2 my-sm-0" type="button" id="loginButton">Suchen</button>
            <div class="btn-group" role="group">
                
    <div class="dropdown dropdown-lg">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
        <div class="dropdown-menu dropdown-menu-right" role="menu">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label for="filter">Filter by</label>
                <select class="form-control">
                    <option value="0" selected>All Snippets</option>
                    <option value="1">Featured</option>
                    <option value="2">Most popular</option>
                    <option value="3">Top rated</option>
                    <option value="4">Most commented</option>
                </select>
              </div>
              
              
              <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
    </div>
    <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
</div>
        </form>
    </div>
</nav>
<script>
document.getElementById("loginButton").onclick = function() {
    window.location.href = "login.php";
}
</script>