<?php
if (isset($_SESSION['recht']) && $_SESSION['recht'] == 2) {
?>
    <div class="container" style="display: flex; ">
        <div class="container fadeInLeft" style="text-align: center;">
            <h3 style="margin-bottom: 10px; font-family: 'Krona One', sans-serif;">User<br>Verwalten</h3>
            <?php
            require_once("inc/db_inc.php");
            require_once("inc/connection.php");

            $query = $db->query("SELECT * FROM admins WHERE rechte=3");
            //Es werden alle normalen User ausgegeben
            echo "<ul class=\"list-group\"><h2>Normale user</h2>";
            foreach ($query as $row) {
                echo "<li class=\"list-group-item\" style=\"font-family: 'Overpass', sans-serif;\">" . $row['benutzername'] . ": " . $row['Vorname'] . " " . $row['Nachname'] . "</li>";
            }
            echo "</ul>";
            //Es werden alle admins ausgegeben
            $query = $db->query("SELECT * FROM admins WHERE rechte=2");

            echo "<ul class=\"list-group\"><h2>Admins</h2>";

            foreach ($query as $row) {
                echo "<li class=\"list-group-item\" style=\"font-family: 'Overpass', sans-serif;\">" . $row['benutzername'] . ": " . $row['Vorname'] . " " . $row['Nachname'] . "</li>";
            }
            echo "</ul>";

            ?>
        </div>
        <div class="container fadeInUp" style="text-align: center;">
            <h3 style="margin-bottom: 10px; font-family: 'Krona One', sans-serif;">Notebooks Hinzufügen</h3>

            <form>
            
                <div class="form-group row">
                    <label for="id" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">ID</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="id" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="marke" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Marke</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="marke">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="modell" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Modell</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="modell">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="aufloesung" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Auflösung</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="aufloesung">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="zoll" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Zoll</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="zoll">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="prozessor" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Prozessor</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="prozessor">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ram" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">RAM</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ram">
                    </div>
                </div>
            </form>
        </div>
        <div class="container fadeInRight" style="text-align: center;">
            <h3 style="margin-bottom: 10px; font-family: 'Krona One', sans-serif;">Notebooks verwalten</h3>

        </div>
    </div>
<?php
} else {
    echo "<h1>Du hast keine Berechtigung dazu</h1>";
}
?>