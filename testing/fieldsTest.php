<link rel="stylesheet" href="../website/Bootstrap/css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="../website/Bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="../website/css/style.css">

<?php
include_once("../website/inc/db_inc.php");
include_once("../website/inc/connection.php");

$query = $db->query("SELECT * FROM notebooks WHERE id=1");

$hermann = 5;
foreach ($query as $row) {
    echo "<div class=\"col-sm-4 fadeInUp\" id=\"sliderCont\">";
    echo "<div id=carousel" . $row['id'] . " class=\" carousel slide\" data-ride=\"carousel\" style=\"margin-top: 50px; min-height: 450px; max-height: 450px;\">";
    echo "<div class=\"carousel-inner\" style=\"min-height:350; max-height: 350;\">";

    for ($i = 1; $i <= $hermann; $i++) {
        echo "<div class=\"carousel-item active\" style=\"max-height: 350px; min-height: 350px;\">";
        echo "<img class=\"d-block w-100\" src=\"" . $row['id'] . "/" . $row['id'] . ".$i.jpg\" alt=\"" . $row['model'] . "\">";
        echo "</div>";
    }

    echo "</div>";
    echo "<a class=\"carousel-control-prev\" href=\"#carousel" . $row['id'] . "\" role=\"button\" data-slide=\"prev\">";
    echo "<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>";
    echo "<span class=\"sr-only\">Previous</span>";
    echo "</a>";
    echo "<a class=\"carousel-control-next\" href=\"#carousel" . $row['id'] . "\" role=\"button\" data-slide=\"next\">";
    echo "<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>";
    echo "<span class=\"sr-only\">Next</span>";
    echo "</a>";
    echo "
        <!-- Kurzer Text-Input-->";
    echo "<h6 id=\"linkNotebook\"><a href=\"nbDetail.php?id=" . $row['id'] . " \">" . $row['model'] . "</a></h6>";
    echo "<p>" . $row['marke'] . "<br>" . $row['preis'] . " CHF</p>";
    echo "</div>";
    echo "</div>";
}
