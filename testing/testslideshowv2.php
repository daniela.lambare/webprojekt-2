<?php
require_once("styles.html");
?>
<div class="item features-image сol-12 col-md-6 col-lg-4">
    <div class="item-wrapper backOfIt">
        <div class="item-img">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="welpe.jpg" class="d-block w-100" alt="Welpe">
                    </div>
                    <div class="carousel-item">
                        <img src="deepdiverlower.png" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="deepdiverupper.png" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="item-content">
            <h5 class="item-title mbr-fonts-style display-7"><strong>Yupppp</strong></h5>

            <p class="mbr-text mbr-fonts-style mt-3 display-7">Try</p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7">hii</p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7">hiii</p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7">Nope</p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7">Yuh</p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7">Naaah</p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7">Rlly</p>
        </div>
        <div class="mbr-section-btn item-footer mt-2"><a href="#" class="btn btn-primary item-btn display-7">More
                &gt;</a></div>
    </div>
</div>

<?php
require_once("scripts.html");
?>