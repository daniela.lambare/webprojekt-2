<!DOCTYPE html>
<html lang="ch-de">

<head>
    <title>Technische Klassen</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Gioele, Daniela, David">
    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <header>
        <?php require_once("Baubloecke/navigation.php"); ?>
    </header>

    <main role="main">

        <section class="jumbotron text-center">
            <div class="container">
                <h1 style="text-emphasis: bold">Technische Klassen</h1>
            </div>
        </section>

            <!-- Slideshow 1-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div id="carousel1" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="bilder/Download.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="bilder/bild.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="bilder/Bild_2.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!-- Kurzer Text-Input-->
                            <h6>Name</h6>
                            <p>Infos</p>
                        </div>     
                    </div>

                    <!--SLideshow-->
                    <div class="col-sm-4">
                        <div id="carousel2" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <img class="d-block w-100" src="bilder/Download.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="bilder/bild.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="bilder/Bild_2.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carousel2" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel2" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!-- Kurzer Text-Input -->
                            <h6>Name</h6>
                            <p>Infos</p>
                        </div>
                    </div>

                    <!--SLideshow 3-->
                    <div class="col-sm-4">
                        <div id="carousel3" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <img class="d-block w-100" src="bilder/Download.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="bilder/bild.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="bilder/Bild_2.jpg" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carousel3" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel3" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!-- Kurzer Text-Input -->
                            <h6>Name</h6>
                            <p>Infos</p>
                        </div>
                    </div>
                </div>
            </div>

    </main>

    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Back to top</a>
            </p>
            <p>&copy;Gioele, Daniela, David</p>
        </div>
    </footer>

    <!-- Einbindung javascripts -->
    <script src="Bootstraps/js/bootstrap.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="Bootstrap/js/bootstrap.min.js"></script>
    
    

</body>

</html>
