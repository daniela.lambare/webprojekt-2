<?php
if (isset($_SESSION['recht']) && $_SESSION['recht'] == 2) {
?>

<!--User verwalten -->
    <div class="container" style="display: flex; ">
        <div class="container fadeInLeft" style="text-align: center;">
            <h3 style="margin-bottom: 10px; font-family: 'Krona One', sans-serif;">User<br>Verwalten</h3>
            <?php
            require_once("inc/db_inc.php");
            require_once("inc/connection.php");

            $query = $db->query("SELECT * FROM admins WHERE rechte=3");
            //Es werden alle normalen User ausgegeben
            echo "<ul class=\"list-group\"><h2>Normale user</h2>";
            foreach ($query as $row) {
                echo "<li class=\"list-group-item\" style=\"font-family: 'Overpass', sans-serif;\">" . $row['benutzername'] . ": " . $row['Vorname'] . " " . $row['Nachname'] . "</li>";
            }
            echo "</ul>";
            //Es werden alle admins ausgegeben
            $query = $db->query("SELECT * FROM admins WHERE rechte=2");

            echo "<ul class=\"list-group\"><h2>Admins</h2>";

            foreach ($query as $row) {
                echo "<li class=\"list-group-item\" style=\"font-family: 'Overpass', sans-serif;\">" . $row['benutzername'] . ": " . $row['Vorname'] . " " . $row['Nachname'] . "</li>";
            }
            echo "</ul>";

            ?>
        </div>

        <!-- Notebooks hinzufügen-->
        <div class="container fadeInUp" style="text-align: center;">
        <div class="container" style="text-align: center;">
            <h3 style="margin-bottom: 10px; font-family: 'Krona One', sans-serif;">Notebooks hinzufügen</h3>

        </div>
            <button type="button" class="btn btn-outline-dark" id="button">Notebooks hinzufügen</button>

            <script type="text/javascript">
                document.getElementById("button").onclick = function () {
                    location.href = "newNotebook.php";
                };
            </script>
                
        </div>
        <div class="container fadeInRight" style="text-align: center;">
            <h3 style="margin-bottom: 10px; font-family: 'Krona One', sans-serif;">Notebooks verwalten</h3>

        </div>
    </div>
<?php
} else {
    echo "<h1>Du hast keine Berechtigung dazu</h1>";
}
?>