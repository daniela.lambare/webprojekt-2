<!DOCTYPE html>
<html lang="ch-de">

<head>
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php
            session_start();
            if (isset($_SESSION['angemeldet']) && $_SESSION['angemeldet']) {
                echo $_SESSION['user'] . " - Admin";
            } else {
                echo "Login - BWZ-Compare";
            }
            ?>

    </title>

    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/animation.css">
    <!--Schriftarten-->
    <link href="https://fonts.googleapis.com/css2?family=Krona+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@1,100&display=swap" rel="stylesheet">

</head>

<body>
    <div style="position: relative;
  min-height: 100vh;">
        <header>
            <!--Navigation-->
            <?php require_once("Baubloecke/navigation.php"); ?>

            <!--Datenbank wird eingebunden-->
            <?php
            require_once("inc/db_inc.php");
            require_once("inc/connection.php");
            ?>
        </header>
        <div style="padding-bottom: 2.5rem; padding-top: 25px;">
            <?php
            if (isset($_SESSION['angemeldet']) && $_SESSION['angemeldet']) {
                echo "<h1 class=\"py-5 fadeInDown\" style=\"text-align: center; font-family: 'Krona One', sans-serif;\">Hallo " . $_SESSION['vornameName'] . "!</h1>";
                if ($_SESSION['recht'] == 2) {
                    require_once("admin/admin.php");
                } elseif ($_SESSION['recht'] == 1) {
                } else {
                }
            } else {
                require_once("login/loginContent.php");
            }
            ?>
        </div>
        <!--Footer einbinden-->
        <?php
        require_once("Baubloecke/footer.php");
        ?>
    </div>
    <!-- Einbindung javascripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="Bootstrap/js/bootstrap.js"></script>
    <script src="Bootstrap/js/bootstrap.min.js"></script>
    <script src="js/button.js"></script>
</body>

</html>