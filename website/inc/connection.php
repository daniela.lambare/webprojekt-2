<?php
$dsn = 'mysql:host=' . $host . ';dbname=' . $database;

//Verbindung zur Datenbank aufbauen
$options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'];
$db = new PDO($dsn, $user, $password, $options);

// Array-Typ für die Datensätze festlegen, assoziativ
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
