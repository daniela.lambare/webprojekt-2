<!DOCTYPE html>
<html lang="de-CH">

<head>
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <title>BWZ-Compare</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animation.css">
</head>

<body>
    <div style="position: relative;
  min-height: 100vh;">
        <!--Navigation-->
        <?php require_once("Baubloecke/navigation.php"); ?>


        <!-- Header -->
        <header class="py-5 bg-image-full fadeInDown"
            style="background-image: url('bilder/notebook.jpg');background-repeat: no-repeat; background-position: center; padding:100px;background-size: cover;">
            <img class="img-fluid d-block mx-auto" src="bilder/unsichtbar.png" alt=""
                style="width: 300px; height: 300px;">
        </header>
        <div style="padding-bottom: 2.5rem;">
            <!-- Content section -->
            <section class="py-5" style="height: 300px">
                <div class="container fadeInLeft">
                    <h1>BWZ-COMPARE</h1>
                    <p class="lead">Vergleichwebseite für das BWZ in Rapperswil-Jona</p>
                </div>
            </section>

            <!-- Image element -->
            <div class="py-5 bg-image-full fadeInUp"
                style="background-image: url('bilder/hintergrund.jpg');background-repeat: no-repeat; background-position: center; padding:100px;background-size: cover;">
                <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
                <div style="height: 400px;"></div>
            </div>

            <!-- Content section -->
            <section class="py-5">
                <div class="container fadeInUp">
                    <h1>WILLKOMMEN</h1>
                    <p class="lead">Willkommen auf unserer Vergleichswebseite von Notebooks für das BWZ. <br>
                        Hier finden Sie viele und passende Geräte, die Sie für die Schule gebrauchen können. <br>
                        Über die Suche können Sie einfach Notebooks suchen und Ihre Suche filtern, sodass Sie bessere
                        Ergebnisse
                        erhalten.</p>
                </div>
            </section>
        </div>
        <!--footer-->
        <?php require_once("Baubloecke/footer.php"); ?>


        <!-- Einbindung javascripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="Bootstrap/js/bootstrap.js"></script>
        <script src="Bootstrap/js/bootstrap.min.js"></script>
        <script src="js/button.js"></script>
</body>

</html>