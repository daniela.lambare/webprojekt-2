<!DOCTYPE html>
<html lang="ch-de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <title>Search - BWZ-Compare</title>

    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animation.css">
</head>

<body>
    <?php
    require_once("Baubloecke/navigation.php");
    require_once("inc/db_inc.php");
    require_once("inc/connection.php");

    $query = $db->query("SELECT * FROM notebooks WHERE model LIKE '%" . $_GET['search'] . "%' OR marke LIKE '%" . $_GET['search'] . "%'");
    ?>
    <div style="padding-bottom: 2.5rem;">
        <main role="main">
            <section class="jumbotron text-center">
                <div class="container fadeInDown">
                    <h1 style="text-emphasis: bold">Suchergebnisse für: <?php echo $_GET['search'] ?></h1>
                </div>
            </section>
            <div class="container">
                <div class="row">
                    <?php


                    foreach ($query as $row) {
                        echo "<div class=\"col-sm-4 fadeInUp\" id=\"sliderCont\">";
                        echo "<div id=carousel" . $row['id'] . " class=\" carousel slide\" data-ride=\"carousel\" style=\"margin-top:50px; min-height: 450px; max-height: 450px;\">";
                        echo "<div class=\"carousel-inner\" style=\"min-height:350; max-height: 350;\">";
                        echo "<div class=\"carousel-item active\" style=\"max-height: 350px; min-height: 350px;\">";
                        echo "<img class=\"d-block w-100\" src=\"bilder/notebooks/" . $row['id'] . "/" . $row['id']
                            . ".1.jpg\" alt=\"" . $row['model'] . "\">";
                        echo "</div>";
                        echo "<div class=\"carousel-item\" style=\"max-height: 350px; min-height: 350px;\">";
                        echo "<img class=\"d-block w-100\" src=\"bilder/notebooks/" . $row['id'] . "/" . $row['id']
                            . ".2.jpg\" alt=\"" . $row['model'] . "\">";
                        echo "</div>";
                        echo "<div class=\"carousel-item\" style=\"max-height: 350px; min-height: 350px;\">";
                        echo "<img class=\"d-block w-100\" src=\"bilder/notebooks/" . $row['id'] . "/" . $row['id']
                            . ".3.jpg\" alt=\"" . $row['model'] . "\">";
                        echo "</div>";
                        echo "<div class=\"carousel-item\" style=\"max-height: 350px; min-height: 350px;\">";
                        echo "<img class=\"d-block w-100\" src=\"bilder/notebooks/" . $row['id'] . "/" . $row['id']
                            . ".4.jpg\" alt=\"" . $row['model'] . "\">";
                        echo "</div>";
                        echo "</div>";
                        echo "<a class=\"carousel-control-prev\" href=\"#carousel" . $row['id'] . "\" role=\"button\"data-slide=\"prev\">";
                        echo "<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>";
                        echo "<span class=\"sr-only\">Previous</span>";
                        echo "</a>";
                        echo "<a class=\"carousel-control-next\" href=\"#carousel" . $row['id'] . "\" role=\"button\"data-slide=\"next\">";
                        echo "<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>";
                        echo "<span class=\"sr-only\">Next</span>";
                        echo "</a>";
                        echo "<!-- Kurzer Text-Input-->";
                        echo "<h6 id=\"linkNotebook\"><a href=\"nbDetail.php?id=" . $row['id'] . " \">" . $row['model'] . "</a></h6>";
                        echo "<p>" . $row['marke'] . "<br>" . $row['preis'] . " CHF</p>";
                        echo "</div>";
                        echo "</div>";
                    }

                    ?>
                </div>
            </div>
        </main>
    </div>
    <!-- Einbindung javascripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="Bootstrap/js/bootstrap.js"></script>
    <script src="Bootstrap/js/bootstrap.min.js"></script>
    <script src="js/button.js"></script>
</body>

</html>