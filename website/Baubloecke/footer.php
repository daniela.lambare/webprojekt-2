<!-- Footer -->
<section class="footer4 cid-s8Ra2I1LkF" once="footers" id="footer4-14">
    <footer class="py-5 bg-dark" style="  position: absolute;
  width: 100%;
  height: 2.5rem; ">
        <div class="container">
            <p class="m-0 text-center text-white">&copy; Gioele, Daniela</p>
            <p class="m-0 text-center text-white">
                <a href="#">Back to top</a>
            </p>
        </div>
    </footer>
</section>