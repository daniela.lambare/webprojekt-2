<!-- Navgiationsmenu -->
<!--Stylesheets-->
<?php
if (session_id() == '') {
    //session has not started
    session_start();
}
?>
<link rel="stylesheet" href="css/style.css">

<!--Logo-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="navigation">
    <a href="https://www.bwz-rappi.ch">
        <img alt="BWZ-Rappi" src="bilder/bwz_logo.png" width="55" height="40">
    </a>
    <a class="navbar-brand" href="home.php">BWZ-Compare</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="home.php">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ueber_uns.php">About</a>
            </li>
            <!--Dropdown-->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Klassen
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="techK.php">IMS</a>
                    <a class="dropdown-item" href="restK.php">Restliche</a>
                </div>
            </li>
            <li>
                <?php
                if (isset($_SESSION['angemeldet']) && $_SESSION['angemeldet'] == true) {
                    echo "<button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" id=\"adminButton\" onclick=\"adminButton()\">Mein Konto</button>";
                    echo "<button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" id=\"loggoutButton\" style=\"margin-left: 3px;\" onclick=\"loggoutButton()\">Logout</button>";
                } else {
                    echo "<button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"button\" id=\"loginButton\" onclick=\"loginButton()\">Login</button>";
                }
                ?>
            </li>
        </ul>

        <!--Suchfeld-->
        <form class="form-inline my-2 my-lg-0" action="search.php" method="GET" name="searchForm">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"
                style="width: 500px;" name="search" autocomplete="off">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="searchButton">Suchen</button>
        </form>
    </div>
</nav>