<style>
    @media screen and (max-width: 600px) {

        #filterMenu {
            display: flex;
            flex-flow: row wrap;
        }

        #filterMenu>* {
            margin-left: 0px;

        }
    }

    @media screen and (min-width: 600px) {
        #filterMenu {
            display: flex;
            flex-direction: row;
        }

        #filterMenu>* {
            margin-left: 10px;
        }

        #filterMenu>*:first-child {
            margin-left: 0px;
        }
    }
</style>
<script type="text/javascript">
    function senden() {
        document.getElementById("formFilter").submit();
    }
</script>
<?php
require_once("inc/db_inc.php");
require_once("inc/connection.php");
?>
<form action="<?php $_SERVER['PHP_SELF'] ?>" method="GET" id="formFilter">
    <div class="container fadeInDown" id="filterMenu">
        <!--Filter Marke-->
        <select class="form-control auswahl" name="marke" onchange="senden()">
            <optgroup>
                <option value="">Marke</option>
                <?php
                $query = $db->query("SELECT marke FROM notebooks GROUP BY marke");

                foreach ($query as $row) {
                    echo "<option  value=\"" . $row['marke'] . "\">" . $row['marke'] . "</option>";
                }
                ?>
            </optgroup>
        </select>

        <!--Filter Auflösung-->
        <select class="form-control auswahl" name="aufloesung" onchange="senden()">
            <optgroup>
                <option value="">Auflösung</option>
                <?php
                $query = $db->query("SELECT aufloesung FROM notebooks GROUP BY aufloesung");

                foreach ($query as $row) {
                    echo "<option  value=\"" . $row['aufloesung'] . "\">" . $row['aufloesung'] . "</option>";
                }
                ?>
            </optgroup>
        </select>


        <!--Filter Zoll-->
        <select class="form-control auswahl" name="zoll" onchange="senden()">
            <optgroup>
                <option value="">Zoll</option>
                <?php
                $query = $db->query("SELECT zoll FROM notebooks GROUP BY zoll");

                foreach ($query as $row) {
                    echo "<option  value=\"" . $row['zoll'] . "\">" . $row['zoll'] . "</option>";
                }
                ?>
            </optgroup>
        </select>

        <!--Filter Prozessor-->
        <select class="form-control auswahl" name="prozessor" onchange="senden()">
            <optgroup>
                <option value="">Prozessor</option>
                <?php
                $query = $db->query("SELECT prozessor FROM notebooks GROUP BY prozessor");

                foreach ($query as $row) {
                    echo "<option  value=\"" . $row['prozessor'] . "\">" . $row['prozessor'] . "</option>";
                }
                ?>
            </optgroup>
        </select>

        <!--Filter RAM-->
        <select class="form-control auswahl" name="ram" onchange="senden()">
            <optgroup>
                <option value="">RAM</option>
                <?php
                $query = $db->query("SELECT RAM FROM notebooks GROUP BY RAM");

                foreach ($query as $row) {
                    echo "<option  value=\"" . $row['RAM'] . "\">" . $row['RAM'] . "</option>";
                }
                ?>
            </optgroup>
        </select>

        <!--Filter Aktivstift-->
        <select class="form-control auswahl" name="aktivstift" onchange="senden()">
            <optgroup>
                <option value="">Aktivstift</option>
                <option value="1">Ja</option>
                <option value="0">Nein</option>

            </optgroup>
        </select>
        <!--SSD-->
        <select class="form-control auswahl" name="ssd" onchange="senden()">
            <optgroup>
                <option value="">SSD</option>
                <?php
                $query = $db->query("SELECT ssd FROM notebooks GROUP BY ssd");

                foreach ($query as $row) {
                    echo "<option  value=\"" . $row['ssd'] . "\">" . $row['ssd'] . "</option>";
                }
                ?>
            </optgroup>
        </select>
        <!--Filter Aktivstift-->
        <select class="form-control auswahl" name="preis" onchange="senden()">
            <optgroup>
                <option value="">Preis</option>
                <option value="ASC">Aufsteigend</option>
                <option value="DESC">Absteigend</option>
            </optgroup>
        </select>
    </div>
</form>