<?php

//Wenn der User auf Einloggen drückt
if (isset($_POST['einloggenButton'])) {
    //Die Eingaben werden gespeichert, ohne spetzielle zeichen gegen hacking
    htmlspecialchars($benutzername = $_POST['benutzername']);
    htmlspecialchars($passwort = $_POST['passwort']);
    //Es wird eine Abfrage für alle Admins gemacht
    $anmeldung = $db->query("SELECT * FROM admins");

    //Das Passwort und der Benutzername werden in ein assiozatives-Array gespeichert
    foreach ($anmeldung as $row) {
        $nutzernamePasswort[$row['benutzername']] = $row['passwort'];
    }
    //Wenn das Passwort und der Nutzername übereinstimmen
    if (isset($nutzernamePasswort[$benutzername]) && $nutzernamePasswort[$benutzername] == $passwort) {
        $_SESSION['angemeldet'] = true;
        $_SESSION['user'] = $benutzername;

        //Vorname und Nachname werden ausgelesen
        $query = $db->query("SELECT Vorname, Nachname FROM admins where benutzername=\"$benutzername\"");

        //Vorname und Nachname werden in eine Session Variable gespeichert
        foreach ($query as $row) {
            $_SESSION['vornameName'] = $row['Vorname'] . " " . $row['Nachname'];
        }

        $recht = $db->prepare("SELECT rechte FROM admins WHERE benutzername=:bn");
        $recht->bindparam(':bn', $benutzername);

        $recht->execute();

        foreach ($recht as $row) {
            $_SESSION['recht'] = $row['rechte'];
        }

        header('Location: login.php');
    } else {
        $falscheingaben = true;
    }
}
?>
<div id="loginWrapper" class="fadeInDown" style="text-align: center;">
    <div id="formContent">

        <!-- Logo -->
        <div class="fadeIn first">
            <img src="bilder/bwz_logo.png" id="icon" alt="BWZ-Logo" />
        </div>

        <!-- Login Seite -->
        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
            <input type="text" id="benutzername" class="fadeIn second" name="benutzername" placeholder="Benutzername">
            <?php
            //Wenn der Nutzer nichts eingibt beim Benutzernamen
            if (isset($_POST['benutzername']) && $_POST['benutzername'] == "" && isset($_POST['einloggenButton'])) {
                echo "<small id=\"errorMessageBenutzername\" class=\"form-text\">Bitte geben Sie einen Benutzernamen ein</small>";
            }
            ?>
            <input type="password" id="passwort" class="fadeIn third" name="passwort" placeholder="Passwort">
            <?php
            //Wenn der Nutzer nichts eingibt beim Passwort
            if (isset($_POST['passwort']) && $_POST['passwort'] == "" && isset($_POST['einloggenButton'])) {
                echo "<small id=\"errorMessagePasswort\" class=\"form-text\">Bitte geben Sie ein Passwort ein</small>";
            }

            ?>
            <input type="submit" class="fadeIn fourth" value="Einloggen" name="einloggenButton">
            <?php
            //Wenn der Nutzer falscheingaben gemacht hat
            if (isset($falscheingaben) && $falscheingaben) {
                echo "<small id=\"errorMessageFalscheingaben\" class=\"form-text\">Falsche login-Informationen</small>";
            }
            ?>
        </form>
        <!-- Registrieren Seite -->
        <div id="formFooter">
            <a class="underlineHover" href="register.php">Registrieren</a>
        </div>

    </div>
</div>
</form>