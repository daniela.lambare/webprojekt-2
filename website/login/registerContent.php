    <?php

    //Die Daten werden gesetzt
    if (isset($_POST['regButton'])) {
        $bn = htmlspecialchars($_POST['benutzername']);
        $pw = htmlspecialchars($_POST['passwort']);
        $vn = htmlspecialchars($_POST['vorname']);
        $nn = htmlspecialchars($_POST['nachname']);

        //Es wird geschaut, ob alle Felder ausgefüllt wurden
        if (!$bn == "" && !$pw == "" && !$vn == "" && !$nn == "") {
            //Die Abfrage wird gemacht
            $usernames = $db->query("SELECT * FROM admins");
            $benutzernamen = array();

            foreach ($usernames as $nutzername) {
                array_push($benutzernamen, $nutzername['benutzername']);
            }

            //Wenn der benutzername nicht schon existiert
            if (!in_array($bn, $benutzernamen)) {

                //Der Benutzer wird angelegt
                $prepSpeichern = $db->prepare("INSERT INTO admins
                VALUES(NULL, :benutzer, :passwort, :vorna, :nachna, 3)");
                $prepSpeichern->bindparam(':benutzer', $bn);
                $prepSpeichern->bindparam(':passwort', $pw);
                $prepSpeichern->bindparam(':vorna', $vn);
                $prepSpeichern->bindparam(':nachna', $nn);

                $prepSpeichern->execute();

                $registered = true;
            } else {
                echo "<script>alert(\"Der Benutzername ist nicht verfügbar\");</script>";
            }
        }
    }
    ?>
    <div class="fadeInDown" id="registerWrapper">
        <div id="formContent">
            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                <!-- Logo -->
                <div class="fadeIn first">
                    <img src="bilder/bwz_logo.png" id="icon" alt="BWZ-Logo" />
                </div>

                <!-- Login Seite -->
                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                    <input type="text" id="benutzername" class="fadeIn second" name="benutzername"
                        placeholder="Benutzernamen eingeben">
                    <?php
                    //Wenn der Nutzer nichts eingibt beim Benutzernamen
                    if (isset($_POST['benutzername']) && $_POST['benutzername'] == "" && isset($_POST['regButton'])) {
                        echo "<small id=\"errorMessageBenutzername\" class=\"form-text\">Bitte geben Sie einen Benutzernamen ein</small>";
                    }
                    ?>
                    <input type="password" id="passwort" class="fadeIn third" name="passwort" placeholder="Passwort">
                    <?php
                    //Wenn der Nutzer nichts eingibt beim Passwort
                    if (isset($_POST['passwort']) && $_POST['passwort'] == "" && isset($_POST['regButton'])) {
                        echo "<small id=\"errorMessagePasswort\" class=\"form-text\">Bitte geben Sie ein Passwort ein</small>";
                    }
                    ?>
                    <input type="text" id="vorname" class="fadeIn third" name="vorname" placeholder="Vornamen eingeben">
                    <?php
                    //Wenn der Nutzer nichts eingibt beim Vorname
                    if (isset($_POST['vorname']) && $_POST['vorname'] == "" && isset($_POST['regButton'])) {
                        echo "<small id=\"errorMessagePasswort\" class=\"form-text\">Bitte geben Sie einen Vornamen ein</small>";
                    }
                    ?>
                    <input type="text" id="nachname" class="fadeIn third" name="nachname"
                        placeholder="Nachnamen eingeben">
                    <?php
                    //Wenn der Nutzer nichts eingibt beim Nachnamen
                    if (isset($_POST['nachname']) && $_POST['nachname'] == "" && isset($_POST['regButton'])) {
                        echo "<small id=\"errorMessagePasswort\" class=\"form-text\">Bitte geben Sie einen Nachnamen ein</small>";
                    }
                    ?>
                    <input type="submit" class="fadeIn fourth" value="Registrieren" name="regButton">
                    <?php
                    //Wenn der Nutzer falscheingaben gemacht hat
                    if (isset($falscheingaben) && $falscheingaben) {
                        echo "<small id=\"errorMessageFalscheingaben\" class=\"form-text\">Falsche login-Informationen</small>";
                    } else if (isset($registered) && $registered) {
                        echo "<small id=\"registeredTrue\" class=\"form-text\">Erfolgreich registriert</small>";
                    }
                    ?>
                    <!-- Login Seite -->
                    <div id="formFooter">
                        <a class="underlineHover" href="login.php">Einloggen</a>
                    </div>
        </div>
    </div>
    </form>