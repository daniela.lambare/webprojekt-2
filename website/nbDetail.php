<!DOCTYPE html>
<html lang="ch-de">
<?php
//Verbindung zur Datenbank
require_once("inc/db_inc.php");
require_once("inc/connection.php");

//Die ID des Notebooks wird geladen
$idNotebook = htmlspecialchars($_GET['id']);

//Es wird ein Array gemacht, in dem Alle Kolumnen namen gespeichert sind
$queryColumn = $db->query("SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='notebooks' AND `TABLE_NAME`='notebooks'");

foreach ($queryColumn as $row) {
    $collumn[] = $row['COLUMN_NAME'];
}

//Es wird ein Array gemacht, mit allen Daten der Notebooks
$queryData = $db->query("SELECT * FROM notebooks WHERE id=$idNotebook");
foreach ($queryData as $row) {
    for ($i = 0; $i < sizeof($collumn); $i++) {
        $dataNB[$collumn[$i]] = $row[$collumn[$i]];
    }
}
?>

<head>
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <title>
        <?php
        //Es wird der Name des Notebooks ausgegeben
        echo $dataNB['model'];
        ?> - BWZ-Compare
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Gioele, Daniela, David">
    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animation.css">

</head>

<body>
    <div style="position: relative;
  min-height: 100vh;">
        <header>
            <?php require_once("Baubloecke/navigation.php"); ?>
        </header>

        <div style="padding-bottom: 2.5rem;">
            <main role="main">

                <section class="jumbotron text-center">
                    <div class="container fadeInDown">
                        <h1 style="text-emphasis: bold">
                            <?php
                            echo $dataNB['model'];
                            ?>
                        </h1>
                    </div>
                </section>

                <div class="container">
                    <div class="row">
                        <div class="col-sm fadeInLeft" style="text-align: center;">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100"
                                            src="bilder/notebooks/<?php echo $dataNB['id'] . "/" . $dataNB['id'] . ".1.jpg" ?>"
                                            alt="<?php echo $dataNB['model'] ?>">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100"
                                            src="bilder/notebooks/<?php echo $dataNB['id'] . "/" . $dataNB['id'] . ".2.jpg" ?>"
                                            alt="Second slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100"
                                            src="bilder/notebooks/<?php echo $dataNB['id'] . "/" . $dataNB['id'] . ".3.jpg" ?>"
                                            alt="Third slide">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div>
                                <div class="card">
                                    <h5 class="card-header">Beschreibung</h5>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title treatment</h5>
                                        <p class="card-text">
                                            <?php
                                            echo $dataNB['beschreibung'];
                                            ?></p>
                                        <h5>Preis:
                                            <?php
                                            echo $dataNB['preis'] . "CHF</h5>";
                                            ?>
                                            <a href="#" class="btn btn-primary">Kaufen</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm fadeInRight" style="text-align: center;">
                            <h3>Technische Daten</h3>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <!--<th scope="col">Hi</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    for ($i = 1; $i < sizeof($collumn) - 1; $i++) {
                                        //Die Sonderzeichen werden ersetzt und Die Spetzifikationen Namen ausgegeben
                                        $replace = array("_");
                                        $specificationName = ucwords(str_replace($replace, " ", $collumn[$i]));
                                        echo "<tr>";
                                        echo "<th scope=\"row\">" . $specificationName . "</th>";
                                        echo "<td>";

                                        //Es wird geschaut, ob eine Spetzifikation dem Entspricht und somit durch wörter ersetzt
                                        if ($collumn[$i] == "aktivstift") {
                                            if ($dataNB[$collumn[$i]] == 1) {
                                                echo "Ja</td></tr>";
                                            } else {
                                                echo "Nein</td></tr>";
                                            }
                                        } elseif ($collumn[$i] == "touchscreen") {
                                            if ($dataNB[$collumn[$i]] == 1) {
                                                echo "Ja</td></tr>";
                                            } else {
                                                echo "Nein</td></tr>";
                                            }
                                        } elseif ($collumn[$i] == "convertible") {
                                            if ($dataNB[$collumn[$i]] == 1) {
                                                echo "Ja</td></tr>";
                                            } else {
                                                echo "Nein</td></tr>";
                                            }
                                        } elseif ($collumn[$i] == "SD") {
                                            if ($dataNB[$collumn[$i]] == 0) {
                                                echo "Kein SD Anschluss</td></tr>";
                                            }
                                        } else {
                                            echo $dataNB[$collumn[$i]];
                                            echo "</td>";
                                            echo "</tr>";
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </main>
        </div>
        <!--footer-->
        <?php require_once("Baubloecke/footer.php"); ?>


        <!-- Einbindung javascripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="Bootstrap/js/bootstrap.js"></script>
        <script src="Bootstrap/js/bootstrap.min.js"></script>
        <script src="js/button.js"></script>

    </div>
</body>

</html>