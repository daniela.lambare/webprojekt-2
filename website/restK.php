<!DOCTYPE html>
<html lang="ch-de">

<head>
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <title>Restliche Klassen - BWZ-Compare</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Gioele, Daniela, David">
    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animation.css">

</head>

<body>
    <div style="position: relative;
  min-height: 100vh;">
        <header>
            <?php require_once("Baubloecke/navigation.php"); ?>
        </header>
        <div style="padding-bottom: 2.5rem;">
            <main role="main">

                <section class="jumbotron text-center">
                    <div class="container fadeInDown">
                        <h1 style="text-emphasis: bold">Restliche Klassen</h1>
                    </div>
                </section>

                <?php require_once("Baubloecke/filter.php");
                require_once("Baubloecke/notebooks.php"); ?>

            </main>
        </div>
        <!--footer-->
        <?php require_once("Baubloecke/footer.php"); ?>

        <!-- Einbindung javascripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="Bootstrap/js/bootstrap.js"></script>
        <script src="Bootstrap/js/bootstrap.min.js"></script>
        <script src="js/button.js"></script>
    </div>

</body>

</html>