<!DOCTYPE html>
<html lang="de-CH">

<head>
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <title>Über Uns - BWZ-Compare</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Einbindung stylesheets -->

    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">

    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animation.css">

</head>

<body>



    <div style="position: relative;
  min-height: 100vh;">
        <header>
            <!-- Navgiationsmenu -->
            <?php require_once("Baubloecke/navigation.php"); ?>

            <!-- Google Maps einbinden-->
            <div>
                <iframe
                    src=https://maps.google.de/maps?hl=de&q=BWZ+%20%20Zürcherstrasse+1+7%20Rapperswil-Jona&t=&z=10&ie=utf8&iwloc=b&output=embed
                    frameborder="0" scrolling="no"
                    style='position: center; size: cover; width: 100%; height:300px ; margin-top: 50px;'
                    class="fadeInDown"></iframe>
            </div>
        </header>

        <div style="padding-bottom: 1.5rem;">
            <section class="jumbotro text-center" style="padding-top: 100px;">
                <div class="container fadeInUp" style="text-align: center; padding-bottom: 50px;">
                    <h1>Über uns</h1>
                </div>
            </section>

            <div class="container fadeInUp" style="display: flex; text-align:center">
                <div class="container">
                    <p style="font-weight: bold;">Adresse</p>
                    <p>Berufsweiterbildungszentrum <br> Zürcherstrasse 1+7 <br> Rapperswil-Jona </p>
                </div>

                <div class="container">
                    <p style="font-weight: bold;">Kontakt</p>
                    <p><a href="mailto:daniela.lambare@bwz-rappi.ch">daniela.lambare@bwz-rappi.ch</a>&nbsp;<br><a
                            href="mailto:gioele.petrillo@bwz-rappi.ch">gioele.petrillo@bwz-rappi.ch</a><br>

                </div>

                <div class="container">
                    <p style="font-weight: bold;">Feedback</p>
                    <p>Sie können uns gerne neue Ideen zu unsere Webseite oder Probleme senden. </p>
                </div>

            </div>

        </div>
        <!-- Footer -->
        <?php
        require_once("Baubloecke/footer.php");
        ?>
    </div>


    <!-- Einbindung javascripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="Bootstrap/js/bootstrap.js"></script>
    <script src="Bootstrap/js/bootstrap.min.js"></script>
    <script src="js/button.js"></script>
</body>

</html>