<!DOCTYPE html>
<html lang="ch-de">

<head>
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <title>Restliche Klassen - BWZ-Compare</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Gioele, Daniela, David">
    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animation.css">

</head>

<body>

<div style="position: relative;
  min-height: 100vh;">
        <header>
            <?php require_once("Baubloecke/navigation.php");
        ?>
        </header>
            <main role="main">

                <section class="jumbotron text-center">
                    <div class="container fadeInDown">
                        <h1 style="text-emphasis: bold">Notebook hinzufügen</h1>
                    </div>
                </section>
            
    <div class="container fadeInUp" style="text-align: center;">
            <form>
                <div class="form-group row">
                    <label for="id" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">ID</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="id" name="id">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="marke" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Marke</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="marke" name="marke">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="modell" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Modell</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="modell" name="modell">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="aufloesung" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Auflösung</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="aufloesung" name="aufloesung">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="zoll" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Zoll</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="zoll" name="zoll">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="prozessor" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Prozessor</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="prozessor" name="prozessor">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ram" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">RAM</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ram" name="ram">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="usb_anzahl" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">USB Anzahl</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="usb_anzahl" name="usb_anzahl">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="usb_c" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">USB-C Anzahl</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="usb_c" name="usb_c">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="windows_typ" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Windows Typ</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="windows_typ" name="windows_typ">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="CPU_kerne" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">CPU Kerne</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="CPU_kerne" name="cpu_kerne">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cpu_tktFrequenz" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">CPU Taktfrequenz</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="cpu_tktFrequenz" name="cpu_tktFrequenz">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="akkuLaufZeit" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Akkulaufzeit</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="akkuLaufZeit" name="akkuLaufZeit">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="stift" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Stift</label>
                    <div class="form-check">
                        <input type="radio" name="inlineRadioOptions1" class="form-check-inline" value="Ja_stift" id="rd_ja">
                        <label class="form-check-label" for="rd_Ja">Ja</label>
                        <input type="radio" name="inlineRadioOptions1" class="form-check-inline" value="Nein_stift" id="rd_Nein">
                        <label class="form-check-label" for="rd_Nein">Nein</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="touchscreen" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Touchscreen</label>
                    <div class="form-check">
                        <input type="radio" name="inlineRadioOptions2" class="form-check-inline" value="Ja_ts" id="rd_ts_ja">
                        <label class="form-check-label" for="rd_ts_Ja">Ja</label>
                        <input type="radio" name="inlineRadioOptions2" class="form-check-inline" value="Nein_ts" id="rd_ts_Nein">
                        <label class="form-check-label" for="rd_ts_Nein">Nein</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="convertible" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Convertible</label>
                    <div class="form-check">
                        <input type="radio" name="inlineRadioOptions3" class="form-check-inline" value="Ja_ct" id="rd_ct_ja">
                        <label class="form-check-label" for="rd_ct_Ja">Ja</label>
                        <input type="radio" name="inlineRadioOptions3" class="form-check-inline" value="Nein_ct" id="rd_ct_Nein">
                        <label class="form-check-label" for="rd_ct_Nein">Nein</label>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="gpu_marke" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">GPU Marke</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="gpu_marke">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="gpu_model" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">GPU Model</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="gpu_model">
                    </div>
                </div>

                <div class="form-group row">
                    <label for=videoAnschluss" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Videoanschluss</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="videoAnschluss">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="sd" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">SD</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="sd">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ssd" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">SSD</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="ssd">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="preis" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">preis</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="preis">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="klasse" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Klasse</label>
                    <div class="form-check">
                        <input type="radio" name="inlineRadioOptions" class="form-check-inline" value="ims" id="rd_ims">
                        <label class="form-check-label" for="rd_ims">IMS</label>
                        <input type="radio" name="inlineRadioOptions" class="form-check-inline" value="restliche" id="restliche">
                        <label class="form-check-label" for="restliche">Restliche</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="beschreibung" class="col-sm-2 col-form-label" style="font-family: 'Overpass', sans-serif;">Beschreibung</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="beschreibung">
                    </div>
                </div>
            </form>

            <button type="button" class="btn btn-outline-dark" id="btnSpeichern" value="btnSpeichern">Speichern</button>

            <?php

            if(isset($_POST['btnSpeichern'])) {

                $Id = $_POST['id'];
                $marke = $_POST['marke'];
                $modell = $_POST['modell'];
                $aufloesung = $_POST['aufloesung'];
                $zoll = $_POST['zoll'];
                $prozessor = $_POST['prozessor'];
                $ram = $_POST['ram'];
                $usb_anzahl = $_POST['usb_anzahl'];
                $usb_c = $_POST['usb_c'];
                $windows_typ = $_POST['windows_typ'];
                $cpu_kerne = $_POST['cpu_kerne'];
                $cpuTktFrequenz = $_POST['cpu_tktFrequenz'];
                $akkuLaufZeit = $_POST['akkuLaufZeit'];

                //Notebook wird gespeichert
                $prepSpeichern = $db->prepare("INSERT INTO admins
                VALUES(NULL, :benutzer, :passwort, :vorna, :nachna, 3)");
                $prepSpeichern->bindparam(':benutzer', $bn);
                $prepSpeichern->bindparam(':passwort', $pw);
                $prepSpeichern->bindparam(':vorna', $vn);
                $prepSpeichern->bindparam(':nachna', $nn);

                $prepSpeichern->execute();
            }

            ?>
    </div>
    </main>

        <!--footer-->
        <?php require_once("Baubloecke/footer.php"); ?>
        </div>
        
        <!-- Einbindung javascripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="Bootstrap/js/bootstrap.js"></script>
        <script src="Bootstrap/js/bootstrap.min.js"></script>
        <script src="js/button.js"></script>
    </div>   

</body>

</html>