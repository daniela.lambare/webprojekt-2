<!DOCTYPE html>
<html lang="ch-de">

<head>
    <link rel="icon" type="image/png" sizes="32x32" href="bilder/bwz_transparent.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrieren - BWZ-Compare</title>

    <!-- Einbindung stylesheets -->
    <link rel="stylesheet" href="Bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/animation.css">
    <link rel="stylesheet" href="css/seitenorganisierung.css">
</head>

<body>
    <header>
        <?php require_once("Baubloecke/navigation.php");
        require_once("inc/db_inc.php");
        require_once("inc/connection.php"); ?>
    </header>
    <div style="margin-top: 55px;">
        <?php
        if (isset($_SESSION['angemeldet']) && $_SESSION['angemeldet']) {
            header('Location: login.php');
            echo "<script>alert(\"Du bist schon angemeldet\");</script>";
        } else {
            require_once("login/registerContent.php");
        }
        ?>
        <div style="margin-top: 55px;">
</body>
<!-- Einbindung javascripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="Bootstrap/js/bootstrap.js"></script>
<script src="Bootstrap/js/bootstrap.min.js"></script>
<script src="js/button.js"></script>

</html>